package hannotte_td_graphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TdGraphqlApplication.class, args);
	}	
	
}
